# OBSOLETE, code consolidated into https://bitbucket.org/emilper/zug-data/

# zug-dumper

basic data dumper to be used to inspect D objects or data structures

## HOW BASIC ?

does not do much yet, not really tested

will change a lot, just starting on it

## TODO

1. How to define a style ? such as json, Perl Data::Dumper, yaml, etc.
    * list separator  -  , 
    * list container  -  []
    * block container -  {}
    * how to mark objects explicitly ? Only Data::Dumper has syntax for that
    * indentation - 2 or 4 or ... characters
    * indentation style - spaces or tabs or something else
    * quote keys ? - bool
    * quote values ? - bool
    * key => value separator: ->, :, => , whatever
    * what quotes to use - ', ", ` etc.
    * sort keys - boolean
    * how to deal with pointers ? should I deal with pointers ?
    * how deep to go with the dumping
    * predefined syles to pick from: json, Data::Dumper, yaml, xml etc.

2. How to test these styles without installing node or Perl or something else ?

3. Do I add dependencies or do I try to make it as self contained as possible ? There is a json module in vibe.d but I really don't want to drag vibe.d into this
